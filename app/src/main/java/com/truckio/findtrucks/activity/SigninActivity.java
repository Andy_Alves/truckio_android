package com.truckio.findtrucks.activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AdditionalUserInfo;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.truckio.findtrucks.R;
import com.truckio.findtrucks.events.success.FetchShipperSuccessEvent;
import com.truckio.findtrucks.events.signin.OTPErrorEvent;
import com.truckio.findtrucks.events.signin.SignInUserEvent;
import com.truckio.findtrucks.events.signin.VerifyPhoneNumberEvent;
import com.truckio.findtrucks.fragments.signin.OTPFragment;
import com.truckio.findtrucks.fragments.signin.PhoneFragment;
import com.truckio.findtrucks.manager.FirestoreManager;
import com.truckio.findtrucks.manager.SharedPrefsManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.truckio.findtrucks.fragments.signin.PhoneFragment.COUNTRY_CODE;

public class SigninActivity extends AppCompatActivity {

    private static final String TAG = "SigninActivity";
    private static final int account_created = 1;

    @BindView(R.id.fl_signin_container)
    FrameLayout container;


    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);
        ButterKnife.bind(this);
        EventBus.getDefault().register(this);

        mAuth = FirebaseAuth.getInstance();

        loadFragment(PhoneFragment.newInstance(), PhoneFragment.TAG);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    public void loadFragment(Fragment fragment, String TAG) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fl_signin_container, fragment, TAG);
        transaction.commit();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == account_created) {
            if (resultCode == RESULT_OK) {
                finish();
            } else {
                Toast.makeText(this, "Error Creating Account!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    //////////////
    ///// EVENTS
    //////////////////

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(VerifyPhoneNumberEvent event) {

        String phonenumber = COUNTRY_CODE + event.phonenumber;

        PhoneAuthProvider.getInstance().verifyPhoneNumber (
                phonenumber,
                60,
                TimeUnit.SECONDS,
                this,
                new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
                    @Override
                    public void onVerificationCompleted(@NonNull PhoneAuthCredential phoneAuthCredential) {
                        // sign the user
                        Log.d(TAG, "onVerificationCompleted: completed");
                        onMessageEvent(new SignInUserEvent(phoneAuthCredential));
                    }

                    @Override
                    public void onVerificationFailed(@NonNull FirebaseException e) {
                        Log.d(TAG, "onVerificationFailed: ", e);
                        // Something went wrong
                    }

                    @Override
                    public void onCodeSent(@NonNull String s, @NonNull PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                        Log.d(TAG, "Verification id : " + s);
                        loadFragment(OTPFragment.newInstance(s), OTPFragment.TAG);
                    }
                });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(SignInUserEvent event) {

        mAuth.signInWithCredential(event.credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");

                            FirebaseUser user = task.getResult().getUser();
                            AdditionalUserInfo userInfo = task.getResult().getAdditionalUserInfo();

                            FirestoreManager.getInstance().userId = user.getUid();
                            FirestoreManager.getInstance().userPhone = user.getPhoneNumber();

                            if (userInfo.isNewUser()) {
                                // sighnup user
                                Intent intent = new Intent(SigninActivity.this, SighUpActivity.class);
                                startActivityForResult(intent, account_created);
                            } else {
                                // login user
                                SharedPrefsManager.saveUserId(SigninActivity.this,user.getUid());
                                FirestoreManager.getInstance().fetchShipper(user.getUid());
                            }

                        } else {
                            // Sign in failed, display a message and update the UI
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                EventBus.getDefault().post(new OTPErrorEvent("Invalid verification code"));
                            }
                        }
                    }
                });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(FetchShipperSuccessEvent event) {
        Intent intent = new Intent(SigninActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

}
