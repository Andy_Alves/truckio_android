package com.truckio.findtrucks.activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.mindorks.paracamera.Camera;
import com.truckio.findtrucks.R;
import com.truckio.findtrucks.enums.PictureType;
import com.truckio.findtrucks.events.failures.UploadKYCFailureEvent;
import com.truckio.findtrucks.events.success.UploadKYCSucessEvent;
import com.truckio.findtrucks.manager.FirestoreManager;
import com.truckio.findtrucks.model.KycShipper;
import com.truckio.findtrucks.views.IndeterminantProgressBar;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import studio.carbonylgroup.textfieldboxes.ExtendedEditText;

public class KYCActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    static final int REQUEST_PAN_FRONT = 1;
    static final int REQUEST_PAN_BACK = 2;
    static final int REQUEST_AADHAR_FRONT = 3;
    static final int REQUEST_AADHAR_BACK = 4;

    static final String PREFIX_PAN_FRONT = "pan_front_";
    static final String PREFIX_PAN_BACK = "pan_back_";
    static final String PREFIX_AADHAR_FRONT = "aadhar_front_";
    static final String PREFIX_AADHAR_BACK = "aadhar_back_";

    String imgPath;

    @BindView(R.id.extented_et_company_name)
    ExtendedEditText editTextCompnayName;

    @BindView(R.id.extented_et_address)
    ExtendedEditText editTextAddress;

    @BindView(R.id.extented_et_pan_number)
    ExtendedEditText editTextPanNumber;

    @BindView(R.id.iv_pan_front)
    ImageView ivPanFront;

    @BindView(R.id.iv_pan_back)
    ImageView ivPanBack;

    @BindView(R.id.iv_aadhar_front)
    ImageView ivAaDharFront;

    @BindView(R.id.iv_aadhar_back)
    ImageView ivAadharBack;

    @BindView(R.id.spn_usertype)
    Spinner spinnerUserType;

    @BindView(R.id.progress_circular__phone)
    IndeterminantProgressBar progressBar;

    @BindView(R.id.tv_verify)
    TextView tvVerify;

    String[] userTypeArray = {"Individual","Shipper", "Transporter", "Broker"};

    Camera camera;
    PictureType picType;
    KycShipper kycShipper;
    String userType;
    String[] imgUri = new String[4];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kyc);
        EventBus.getDefault().register(this);
        ButterKnife.bind(this);

        spinnerUserType.setOnItemSelectedListener(this);
        ArrayAdapter aa = new ArrayAdapter(this,android.R.layout.simple_spinner_item, userTypeArray);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerUserType.setAdapter(aa);
        spinnerUserType.setSelection(0);

    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        camera.deleteImage();
        super.onDestroy();
    }

    public void openCamera(String prefix) {

        camera = new Camera.Builder()
                .resetToCorrectOrientation(true)// it will rotate the camera bitmap to the correct orientation from meta data
                .setTakePhotoRequestCode(1)
                .setDirectory("truckio")
                .setName(prefix + FirestoreManager.getInstance().shipper.user_id)
                .setImageFormat(Camera.IMAGE_JPG)
                .setCompression(75)
                .setImageHeight(1000)// it will try to achieve this height as close as possible maintaining the aspect ratio;
                .build(this);

        try {
            camera.takePicture();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public boolean validate() {

        if (userType == null) {
            makeToast("Select User Type");
            return false;
        }

        if (editTextCompnayName.getText().toString().isEmpty()) {
            editTextCompnayName.setError("Company name cannot be empty");
            return false;
        } else if (editTextCompnayName.getText().toString().length() < 5) {
            editTextCompnayName.setError("Company name must be minimum 5 characters");
            return false;
        }

        if (editTextAddress.getText().toString().isEmpty()) {
            editTextAddress.setError("please enter address");
            return false;
        } else if (editTextAddress.getText().toString().length() < 15) {
            editTextAddress.setError("Address must be minimum 15 characters");
            return false;
        }

        if (editTextPanNumber.getText().toString().isEmpty()) {
            editTextPanNumber.setError("please enter PAN");
            return false;
        } else if (editTextPanNumber.getText().toString().length() != 10) {
            editTextPanNumber.setError("PAN must be 10 characters");
            return false;
        }

        if (imgUri[0] == null) {
            makeToast("Please upload PAN front page");
            return false;
        }
        if (imgUri[1] == null) {
            makeToast("Please upload PAN back page");
            return false;
        }
        if (imgUri[2] == null) {
            makeToast("Please upload Aadhar front page");
            return false;
        }
        if (imgUri[3] == null) {
            makeToast("Please upload Aadhar back page");
            return false;
        }

        return true;
    }

    public void makeToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == Camera.REQUEST_TAKE_PHOTO){
            Bitmap bitmap = null;

            File imgFile = new  File(camera.getCameraBitmapPath());

            if(imgFile.exists()){
                bitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
            }

            if(bitmap != null && !bitmap.isRecycled()) {
                switch (picType) {
                    case PAN_FRONT:
                        imgUri[0] = camera.getCameraBitmapPath();
                        ivPanFront.setImageBitmap(bitmap);
                        break;
                    case PAN_BACK:
                        imgUri[1] = camera.getCameraBitmapPath();
                        ivPanBack.setImageBitmap(bitmap);
                        break;
                    case AADHAR_FRONT:
                        imgUri[2] = camera.getCameraBitmapPath();
                        ivAaDharFront.setImageBitmap(bitmap);
                        break;
                    case AADHAR_BACK:
                        imgUri[3] = camera.getCameraBitmapPath();
                        ivAadharBack.setImageBitmap(bitmap);
                        break;
                }

            } else {
                Toast.makeText(this.getApplicationContext(),"Picture not taken!",Toast.LENGTH_SHORT).show();
            }
        }
    }

    @OnClick(R.id.btn_pan_front)
    public void clickPanFront() {
        openCamera(PREFIX_PAN_FRONT);
        picType = PictureType.PAN_FRONT;
    }

    @OnClick(R.id.btn_pan_back)
    public void clickPanBack() {
        openCamera(PREFIX_PAN_BACK);
        picType = PictureType.PAN_BACK;
    }

    @OnClick(R.id.btn_aadhar_front)
    public void clickAadharFront() {
        openCamera(PREFIX_AADHAR_FRONT);
        picType = PictureType.AADHAR_FRONT;
    }

    @OnClick(R.id.btn_aadhar_back)
    public void clickAadharBack() {
        openCamera(PREFIX_AADHAR_BACK);
        picType = PictureType.AADHAR_BACK;
    }

    @OnClick(R.id.btn_verify__kyc)
    public void clickVerify() {
        if (validate()) {

            progressBar.setVisibility(View.VISIBLE);
            tvVerify.setVisibility(View.GONE);

            kycShipper = new KycShipper();
            kycShipper.userId = FirestoreManager.getInstance().shipper.user_id;
            kycShipper.userType = userType;
            kycShipper.companyName = editTextCompnayName.getText().toString();
            kycShipper.address = editTextAddress.getText().toString();
            kycShipper.panId = editTextPanNumber.getText().toString();

            FirestoreManager.getInstance().uploadKYC(kycShipper, imgUri);
        }
    }

    @OnClick(R.id.ib_back__kyc)
    public void clickBack() {
        finish();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        userType = userTypeArray[position];
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(UploadKYCSucessEvent event) {
        progressBar.setVisibility(View.GONE);
        tvVerify.setVisibility(View.VISIBLE);
        makeToast("KYC completed! Welcome to truckio :)");
        finish();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(UploadKYCFailureEvent event) {
        progressBar.setVisibility(View.GONE);
        tvVerify.setVisibility(View.VISIBLE);
        makeToast("Something went wrong!");

    }
}
