package com.truckio.findtrucks.activity;


import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.truckio.findtrucks.R;
import com.truckio.findtrucks.adapters.MainViewPagerAdapter;
import com.truckio.findtrucks.enums.MainTabs;
import com.truckio.findtrucks.events.GoToPageEvent;
import com.truckio.findtrucks.events.LogoutEvent;
import com.truckio.findtrucks.manager.SharedPrefsManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Andrews on 10,August,2019
 */

public class MainActivity extends AppCompatActivity {

    // UI
    @BindView(R.id.ib_requests)
    ImageButton ibRequests;

    @BindView(R.id.ib_posttrucks)
    ImageButton ibPostTrucks;

    @BindView(R.id.ib_account)
    ImageButton ibAccount;

    @BindView(R.id.vp_main)
    ViewPager mainViewPager;

    MainViewPagerAdapter viewPagerAdapter;
    MainTabs mainTabs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
        EventBus.getDefault().register(this);

        viewPagerAdapter = new MainViewPagerAdapter(getSupportFragmentManager());
        mainViewPager.setAdapter(viewPagerAdapter);

        mainViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {

                switch (i) {
                    case 0:
                        mainTabs = MainTabs.REQUEST_TAB;
                        break;
                    case 1:
                        mainTabs = MainTabs.POST_TRUCK_TAB;
                        break;
                    case 2:
                        mainTabs = MainTabs.ACCOUNT_TAB;
                        break;

                }

                updateTabUi(mainTabs);
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

        clickRequests();

    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @OnClick(R.id.ib_requests)
    public void clickRequests() {
        mainTabs = MainTabs.REQUEST_TAB;
        mainViewPager.setCurrentItem(0);
        updateTabUi(mainTabs);
    }

    @OnClick(R.id.ib_posttrucks)
    public void clickPostTrucks() {
        mainTabs = MainTabs.POST_TRUCK_TAB;
        mainViewPager.setCurrentItem(1);
        updateTabUi(mainTabs);
    }

    @OnClick(R.id.ib_account)
    public void clickAccount() {
        mainTabs = MainTabs.ACCOUNT_TAB;
        mainViewPager.setCurrentItem(2);
        updateTabUi(mainTabs);
    }

    public void updateTabUi(MainTabs tabs) {

        ibRequests.setImageResource(R.drawable.ic_tab_requests_off);
        ibPostTrucks.setImageResource(R.drawable.ic_tab_truck_off);
        ibAccount.setImageResource(R.drawable.ic_tab_account_off);

        switch (tabs) {
            case REQUEST_TAB:
                ibRequests.setImageResource(R.drawable.ic_tab_requests_on);
                break;

            case POST_TRUCK_TAB:
                ibPostTrucks.setImageResource(R.drawable.ic_tab_truck_on);
                break;

            case ACCOUNT_TAB:
                ibAccount.setImageResource(R.drawable.ic_tab_account_on);
                break;
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(GoToPageEvent event) {
        switch (event.mainTabs) {
            case REQUEST_TAB:
                clickRequests();
                break;

            case POST_TRUCK_TAB:
                clickPostTrucks();
                break;

            case ACCOUNT_TAB:
                clickAccount();
                break;
        }

        clickPostTrucks();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(LogoutEvent event) {
        SharedPrefsManager.saveUserId(this, null);
        Intent intent = new Intent(MainActivity.this, SigninActivity.class);
        startActivity(intent);
        Toast.makeText(this, "Logged out! We see you Soon :)", Toast.LENGTH_SHORT).show();
        finish();
    }

}
