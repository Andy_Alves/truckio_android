package com.truckio.findtrucks.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.truckio.findtrucks.R;
import com.truckio.findtrucks.events.signin.AccountCreatedResultEvent;
import com.truckio.findtrucks.manager.FirestoreManager;
import com.truckio.findtrucks.model.Shipper;
import com.truckio.findtrucks.views.IndeterminantProgressBar;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import studio.carbonylgroup.textfieldboxes.ExtendedEditText;

public class SighUpActivity extends AppCompatActivity {

    @BindView(R.id.extented_et_name)
    ExtendedEditText editTextName;

    @BindView(R.id.extented_et_company_name)
    ExtendedEditText editTextCompany;

    @BindView(R.id.extented_et_city_name)
    ExtendedEditText editTextCity;

    @BindView(R.id.progress_circular__phone)
    IndeterminantProgressBar progressBar;

    @BindView(R.id.tv_next)
    TextView tvNext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @OnClick(R.id.btn_next__signup)
    public void clickNext() {

        if (!validate()) {
            return;
        }

        progressBar.setVisibility(View.VISIBLE);
        tvNext.setVisibility(View.GONE);

        Shipper shipper = new Shipper();
        shipper.user_id = FirestoreManager.getInstance().userId;
        shipper.name = editTextName.getText().toString();
        shipper.company_name = editTextCompany.getText().toString();
        shipper.city = editTextCity.getText().toString();
        shipper.phone = FirestoreManager.getInstance().userPhone;

        FirestoreManager.getInstance().signUpUser(this, shipper);
    }

    private boolean validate() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(AccountCreatedResultEvent event) {
        if (event.isSuccess) {
            Toast.makeText(this, "Account Created successfully!", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(SighUpActivity.this, MainActivity.class);
            startActivity(intent);
            setResult(RESULT_OK);
            finish();
        } else {
            setResult(RESULT_CANCELED);
            finish();
        }
    }

}
