package com.truckio.findtrucks.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.truckio.findtrucks.R;
import com.truckio.findtrucks.Utils;
import com.truckio.findtrucks.enums.MaterialType;
import com.truckio.findtrucks.enums.TruckType;
import com.truckio.findtrucks.events.MaterialSelectedEvent;
import com.truckio.findtrucks.events.PlaceSelectedEvent;
import com.truckio.findtrucks.events.TruckSelectedEvent;
import com.truckio.findtrucks.fragments.dialogfragment.SelectMaterialFragment;
import com.truckio.findtrucks.fragments.dialogfragment.SelectTruckFragment;
import com.truckio.findtrucks.manager.FirestoreManager;
import com.truckio.findtrucks.model.TruckRequest;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EditRequestActivity extends AppCompatActivity implements View.OnClickListener, TimePickerDialog.OnTimeSetListener, DatePickerDialog.OnDateSetListener {

    public static final String TAG = "EditRequestActivity";
    public static final String KEY_EDITING_REQUEST = "edit_request_index";
    /** ButterKnife Code **/

    @BindView(R.id.tv_pickpoint)
    TextView tvPickpoint;
    @BindView(R.id.ib_clear_text1)
    ImageButton clearText1;

    @BindView(R.id.tv_droppoint1)
    TextView tvDroppoint1;
    @BindView(R.id.ib_clear_text2)
    ImageButton ibClearText2;

    @BindView(R.id.ll_droppoint2)
    LinearLayout lldroppoint2;
    @BindView(R.id.tv_droppoint2)
    TextView tvDroppoint2;
    @BindView(R.id.ib_clear_text3)
    ImageButton ibClearText3;

    @BindView(R.id.ll_droppoint3)
    LinearLayout llDroppoint3;
    @BindView(R.id.tv_droppoint3)
    TextView tvDroppoint3;
    @BindView(R.id.ib_clear_text4)
    ImageButton ibClearText4;

    @BindView(R.id.ll_add_point)
    LinearLayout llAddPoint;
    @BindView(R.id.ib_add_point)
    ImageButton ibAddPoint;

    @BindView(R.id.btn_select_material)
    LinearLayout btnSelectMaterial;
    @BindView(R.id.btn_select_truck)
    LinearLayout btnSelectTruck;
    @BindView(R.id.btn_pickup_time)
    LinearLayout btnPickupTime;

    @BindView(R.id.tv_materialname)
    TextView tvMateiralName;
    @BindView(R.id.tv_truckname)
    TextView tvTruckName;
    @BindView(R.id.tv_Month)
    TextView tvMonth;
    @BindView(R.id.tv_Time)
    TextView tvTime;

    @BindView(R.id.ib_ic_material)
    ImageView ivMaterial;

    @BindView(R.id.ib_ic_truck)
    ImageView ivTruck;

    /** ButterKnife Code **/

    boolean haveDroppoint2;
    boolean haveDroppoint3;

    int selectedPointForLocation;

    public TruckRequest requestModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_request);
        ButterKnife.bind(this);

        tvPickpoint.setOnClickListener(this);
        tvDroppoint1.setOnClickListener(this);
        tvDroppoint2.setOnClickListener(this);
        tvDroppoint3.setOnClickListener(this);

        EventBus.getDefault().register(this);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            int index = bundle.getInt(KEY_EDITING_REQUEST);
            requestModel = FirestoreManager.getInstance().truckRequests.get(index);
            updateUI();
        }
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    public Calendar getStartDate() {
        Calendar output = Calendar.getInstance();
        output.set(Calendar.YEAR, Calendar.getInstance().get(Calendar.YEAR));
        output.set(Calendar.DAY_OF_MONTH, Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
        output.set(Calendar.MONTH, Calendar.getInstance().get(Calendar.MONTH));
        return output;
    }

    public Calendar getEndDate() {
        Calendar output2 = Calendar.getInstance();
        output2.set(Calendar.YEAR, 2020);
        output2.set(Calendar.DAY_OF_MONTH, Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
        output2.set(Calendar.MONTH, Calendar.getInstance().get(Calendar.MONTH));
        return output2;
    }

    public void showDatePicker() {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                EditRequestActivity.this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );

        dpd.setAccentColor(getResources().getColor(R.color.truckio_orange));
        dpd.setMinDate(getStartDate());
        dpd.setMaxDate(getEndDate());
        dpd.show(getSupportFragmentManager(), "Datepickerdialog");
    }

    public void showTimePicker() {
        Date date = new Date();
        date.getTime();
        Calendar now = Calendar.getInstance();
        TimePickerDialog dpd = TimePickerDialog.newInstance(
                EditRequestActivity.this,
                now.get(Calendar.HOUR),
                now.get(Calendar.MINUTE),
                false
        );

        dpd.setAccentColor(getResources().getColor(R.color.truckio_orange));
        dpd.show(getSupportFragmentManager(), "Datepickerdialog");
    }

    public boolean isValidated() {

        if (requestModel.placeId1 == null) {
            makeToast("Pickup Point cannot be empty");
            return false;
        } else if (requestModel.placeId2 == null) {
            makeToast("Drop Point cannot be empty");
            return false;
        }

        if (requestModel.material_type == null) {
            makeToast("Select Material");
            return false;
        } else if (requestModel.truck_type == null) {
            makeToast("Select Truck");
            return false;
        } else if (requestModel.getMonth() == 0 || requestModel.getDate() == 0) {
            makeToast("Select PickUp Date and Time");
            return false;
        }

        return true;
    }

    public void makeToast(String text) {

        Toast toast = Toast.makeText(this, text, Toast.LENGTH_SHORT);
        View view = toast.getView();
        view.getBackground().setColorFilter(getResources().getColor(R.color.red), PorterDuff.Mode.SRC_IN);

        TextView text1 = view.findViewById(android.R.id.message);
        text1.setTextColor(getResources().getColor(R.color.white));

        toast.show();
    }

    ///////////////////////
    ////// CLICK MAIN
    //////////////////////

    @OnClick(R.id.ib_add_point)
    public void clickAddDroppoints() {
        if (haveDroppoint2) {
            haveDroppoint3 = true;
            llDroppoint3.setVisibility(View.VISIBLE);
            ibClearText4.setImageResource(R.drawable.ic_delete);
        } else {
            haveDroppoint2 = true;
            lldroppoint2.setVisibility(View.VISIBLE);
            ibClearText3.setImageResource(R.drawable.ic_delete);
        }
    }

    @OnClick(R.id.btn_select_material)
    public void clickMaterial() {
        SelectMaterialFragment.newInstance().show(getSupportFragmentManager(), "SelectMaterialFragment");
    }

    @OnClick(R.id.btn_select_truck)
    public void clickTrucks() {
        SelectTruckFragment.newInstance(requestModel.truck_type, requestModel.tons).show(getSupportFragmentManager(), "SelectTruckFragment");
    }

    @OnClick(R.id.btn_pickup_time)
    public void clickPickUpTime() {
        showDatePicker();
    }

    @OnClick(R.id.btn_save)
    public void clickSave() {
        if (isValidated()) {
            FirestoreManager.getInstance().postTruckRequest(this, requestModel);
            finish();
        }
    }

    @OnClick(R.id.btn_delete)
    public void clickDelete() {
        if (isValidated()) {
            FirestoreManager.getInstance().deleteTruckRequest(requestModel);
            finish();
        }
    }

    @OnClick(R.id.ib_close__editrequest)
    public void clickClose() {
       finish();
    }


    ///////////////////////
    /// CLEAR POINT CLICKS
    //////////////////////

    @OnClick(R.id.ib_clear_text1)
    public void clickClearText1() {
        requestModel.placeId1 = null;
        updateUI();
    }

    @OnClick(R.id.ib_clear_text2)
    public void clickClearText2() {
        requestModel.placeId2 = null;
        updateUI();
    }

    @OnClick(R.id.ib_clear_text3)
    public void clickClearText3() {
        if (tvDroppoint2.getText().toString().isEmpty()) {
            lldroppoint2.setVisibility(View.GONE);
            haveDroppoint2 = false;
        } else {
            requestModel.placeId3 = null;
            ibClearText3.setImageResource(R.drawable.ic_delete);
            updateUI();
        }
    }

    @OnClick(R.id.ib_clear_text4)
    public void clickClearText4() {
        if (tvDroppoint3.getText().toString().isEmpty()) {
            llDroppoint3.setVisibility(View.GONE);
            haveDroppoint3 = false;
        } else {
            requestModel.placeId4 = null;
            ibClearText4.setImageResource(R.drawable.ic_delete);
            updateUI();
        }
    }

    @Override
    public void onClick(View v) {

        if (v == tvPickpoint) {
            selectedPointForLocation = 0;
        } else  if (v == tvDroppoint1) {
            selectedPointForLocation = 1;
        } else  if (v == tvDroppoint2) {
            selectedPointForLocation = 2;
        } else  if (v == tvDroppoint3) {
            selectedPointForLocation = 3;
        }

        Intent intent = new Intent(this, SelectLocationActivity.class);
        startActivity(intent);
    }

    public void updateUI() {

        if (requestModel.placeId1 != null) {
            tvPickpoint.setText(requestModel.pickup_point);
        } else {
            tvPickpoint.setHint(R.string.app_frag_posttruck_pickpoint);
            tvPickpoint.setText("");
        }

        if (requestModel.placeId2 != null) {
            tvDroppoint1.setText(requestModel.drop_point1);
        } else {
            tvDroppoint1.setHint(R.string.app_frag_posttruck_droppoint1);
            tvDroppoint1.setText("");
        }

        if (requestModel.placeId3 != null) {
            tvDroppoint2.setText(requestModel.drop_point2);
            ibClearText3.setImageResource(R.drawable.ic_close);
        } else {
            tvDroppoint2.setHint(R.string.app_frag_posttruck_droppoint2);
            tvDroppoint2.setText("");
        }

        if (requestModel.placeId4 != null) {
            tvDroppoint3.setText(requestModel.drop_point3);
            ibClearText4.setImageResource(R.drawable.ic_close);
        } else {
            tvDroppoint3.setHint(R.string.app_frag_posttruck_droppoint3);
            tvDroppoint3.setText("");
        }

        if(requestModel.truck_type == null) {
            ivTruck.setImageResource(R.drawable.ic_truck_simple);
            tvTruckName.setText("Select Truck");
        } else {
            TruckType truckType = TruckType.valueOf(requestModel.truck_type);
            switch (truckType) {
                case OPEN:
                    ivTruck.setImageResource(R.drawable.ic_truck_open);
                    tvTruckName.setText(requestModel.tons + " Ton's" + " Open");
                    break;
                case CONTAINER:
                    ivTruck.setImageResource(R.drawable.ic_container);
                    tvTruckName.setText(requestModel.tons + " Ton's" + " Container");
                    break;
                case TRAILER:
                    ivTruck.setImageResource(R.drawable.ic_truck_trailer);
                    tvTruckName.setText(requestModel.tons + " Ton's" + " Trailer");
                    break;
            }
        }

        if (requestModel.material_type == null) {
            ivMaterial.setImageResource(R.drawable.ic_box);
            tvMateiralName.setText("Select Material");
        } else {
            MaterialType materialType = MaterialType.valueOf(requestModel.material_type);
            switch (materialType) {
                case FOOD:
                    ivMaterial.setImageResource(R.drawable.ic_food);
                    tvMateiralName.setText("Food & Agri");
                    break;
                case ELECTRONICS:
                    ivMaterial.setImageResource(R.drawable.ic_electronics);
                    tvMateiralName.setText("Electronics");
                    break;
                case FMCG:
                    ivMaterial.setImageResource(R.drawable.ic_fmcg);
                    tvMateiralName.setText("FMCG");
                    break;
                case MEDICINE:
                    ivMaterial.setImageResource(R.drawable.ic_medicine);
                    tvMateiralName.setText("Medicine");
                    break;
                case BUILDING_MATERIALS:
                    ivMaterial.setImageResource(R.drawable.ic_building_material);
                    tvMateiralName.setText("Building Material");
                    break;
                case SPARES:
                    ivMaterial.setImageResource(R.drawable.ic_spares);
                    tvMateiralName.setText("Spares");
                    break;
            }
        }

        if (requestModel.getMonth() != 0 && requestModel.getDate() != 0) {
            String month = Utils.getMonthString(requestModel.getMonth() ) + " " + requestModel.getDate();
            tvMonth.setText(month);

            int hours = requestModel.getHours();
            String meridien = " am";

            if (requestModel.getHours() > 12) {
                hours = requestModel.getHours() - 12;
                meridien = " pm";
            }

            // format to two decimal
            String hour =  new DecimalFormat("00").format(hours);
            String min =  new DecimalFormat("00").format(requestModel.getMinutes());
            String time = hour + ":" + min + meridien;

            tvTime.setText(time);
        }

    }

    /////////////////////////////////
    ////////// EVENTS
    ////////////////////////////////

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(PlaceSelectedEvent event) {

        switch (selectedPointForLocation) {
            case 0:
                requestModel.setPickUpPoint(event.place);
                break;
            case 1:
                requestModel.setDropPoint1(event.place);
                break;
            case 2:
                requestModel.setDropPoint2(event.place);
                break;
            case 3:
                requestModel.setDropPoint3(event.place);
                break;
        }

        updateUI();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(TruckSelectedEvent event) {
        requestModel.setTons(event.tons);
        requestModel.setTruck_type(event.truckType.toString());
        updateUI();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MaterialSelectedEvent event) {
        requestModel.setMaterial_type(event.materialType.toString());
        updateUI();
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        requestModel.setPickUpDate(dayOfMonth, monthOfYear + 1);
        Log.e(TAG, "onDateSet: " + monthOfYear + " : " + dayOfMonth);
        showTimePicker();
    }

    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
        requestModel.setPickUpTime(hourOfDay, minute);
        Log.e(TAG, "onTimeSet: " + hourOfDay + " : " + minute);
        updateUI();
    }
}
