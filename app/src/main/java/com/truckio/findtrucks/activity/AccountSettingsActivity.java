package com.truckio.findtrucks.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.truckio.findtrucks.R;
import com.truckio.findtrucks.events.success.AccountSettingsSaveSuccessEvent;
import com.truckio.findtrucks.manager.FirestoreManager;
import com.truckio.findtrucks.model.Shipper;
import com.truckio.findtrucks.views.IndeterminantProgressBar;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import studio.carbonylgroup.textfieldboxes.ExtendedEditText;

public class AccountSettingsActivity extends AppCompatActivity {

    @BindView(R.id.extented_et_name)
    ExtendedEditText editTextName;

    @BindView(R.id.extented_et_company_name)
    ExtendedEditText editTextCompany;

    @BindView(R.id.extented_et_city_name)
    ExtendedEditText editTextCity;

    @BindView(R.id.progress_circular__accountsettings)
    IndeterminantProgressBar progressBar;

    @BindView(R.id.tv_save__accountsettings)
    TextView tvNext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_settings);
        ButterKnife.bind(this);
        EventBus.getDefault().register(this);

        editTextName.setText(FirestoreManager.getInstance().shipper.name);
        editTextCompany.setText(FirestoreManager.getInstance().shipper.company_name);
        editTextCity.setText(FirestoreManager.getInstance().shipper.city);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @OnClick(R.id.ib_back__accountsettings)
    public void clickBack() {
        finish();
    }

    @OnClick(R.id.btn_save__accountsettings)
    public void clickSave() {

        if(validate()) {
            progressBar.setVisibility(View.VISIBLE);
            tvNext.setVisibility(View.GONE);

            Shipper shipper = FirestoreManager.getInstance().getShipper();
            shipper.name = editTextName.getText().toString();
            shipper.company_name = editTextCompany.getText().toString();
            shipper.city = editTextCity.getText().toString();
            FirestoreManager.getInstance().updateShipperInfo(shipper);
        }
    }

    public boolean validate() {

        if (editTextName.getText().length() < 4) {
            editTextName.setError("Name must be minimun 4 characters");
            return false;
        }

        if (editTextCity.getText().length() < 4) {
            editTextName.setError("City must be minimun 4 characters");
            return false;
        }

        if (editTextCompany.getText().length() < 4) {
            editTextName.setError("City must be minimun 4 characters");
            return false;
        }

        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(AccountSettingsSaveSuccessEvent event) {
        finish();
    }
}
