package com.truckio.findtrucks.customs;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import com.wdullaer.materialdatetimepicker.date.DateRangeLimiter;

import java.util.Calendar;


/**
 * Created by Andrews on 20,August,2019
 */
public class DateLimiter implements DateRangeLimiter {

    public DateLimiter(Parcel in) {

    }

    @NonNull
    @Override
    public Calendar getStartDate() {
        Calendar output = Calendar.getInstance();
        output.set(Calendar.YEAR, 2019);
        output.set(Calendar.DAY_OF_MONTH, 20);
        output.set(Calendar.MONTH, Calendar.AUGUST);
        return output;
    }

    @Override
    public int getMinYear() {
        return 2019;
    }

    @Override
    public int getMaxYear() {
        return 2019;
    }

    @NonNull
    @Override
    public Calendar getEndDate() {
        return null;
    }

    @Override
    public boolean isOutOfRange(int year, int month, int day) {
        return false;
    }

    @NonNull
    @Override
    public Calendar setToNearestDate(@NonNull Calendar day) {
        return day;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }

    public static final Parcelable.Creator<DateLimiter> CREATOR
            = new Parcelable.Creator<DateLimiter>() {
        public DateLimiter createFromParcel(Parcel in) {
            return new DateLimiter(in);
        }

        public DateLimiter[] newArray(int size) {
            return new DateLimiter[size];
        }
    };
}
