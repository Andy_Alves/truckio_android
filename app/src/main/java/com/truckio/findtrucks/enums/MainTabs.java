package com.truckio.findtrucks.enums;

/**
 * Created by Andrews on 10,August,2019
 */

public enum MainTabs {
    REQUEST_TAB(0),
    POST_TRUCK_TAB(1),
    ACCOUNT_TAB(2);

    private final int value;
    private MainTabs(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
