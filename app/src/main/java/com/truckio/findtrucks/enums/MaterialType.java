package com.truckio.findtrucks.enums;

/**
 * Created by Admin on 19,August,2019
 */
public enum  MaterialType {
    FOOD,
    ELECTRONICS,
    FMCG,
    MEDICINE,
    BUILDING_MATERIALS,
    SPARES
}
