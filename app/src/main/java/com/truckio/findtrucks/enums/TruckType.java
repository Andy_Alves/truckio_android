package com.truckio.findtrucks.enums;

/**
 * Created by Andrews on 19,August,2019
 */
public enum TruckType {
    OPEN(0),
    CONTAINER(1),
    TRAILER(2);

    private final int value;
    private TruckType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

}
