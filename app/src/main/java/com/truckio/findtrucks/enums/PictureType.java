package com.truckio.findtrucks.enums;

/**
 * Created by Andrews on 09,September,2019
 */
public enum  PictureType {
    PAN_FRONT,
    PAN_BACK,
    AADHAR_FRONT,
    AADHAR_BACK
}
