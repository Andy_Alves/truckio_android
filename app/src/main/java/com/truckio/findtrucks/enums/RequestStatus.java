package com.truckio.findtrucks.enums;

/**
 * Created by Admin on 04,September,2019
 */
public enum  RequestStatus {
    PENDING,
    TRUCK_AVAILABLE,
    EXPIRED,
}
