package com.truckio.findtrucks;

import android.content.Intent;
import android.os.Handler;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.truckio.findtrucks.activity.MainActivity;
import com.truckio.findtrucks.activity.SigninActivity;
import com.truckio.findtrucks.events.success.FetchShipperSuccessEvent;
import com.truckio.findtrucks.manager.FirestoreManager;
import com.truckio.findtrucks.manager.SharedPrefsManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class Splash extends AppCompatActivity {

    private static final String TAG = "Splash";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        EventBus.getDefault().register(this);

        Handler openMainHandler = new Handler();

        openMainHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                validateSignIn();
            }
        }, 2000);
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    public void validateSignIn() {

        String userId = SharedPrefsManager.getShipperId(this);

        if (userId != null ) {
            FirestoreManager.getInstance().userId = userId;
            FirestoreManager.getInstance().fetchShipper(userId);
            return;
        }

        Intent intent = new Intent(Splash.this, SigninActivity.class);
        startActivity(intent);
        finish();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(FetchShipperSuccessEvent event) {
        Intent intent = new Intent(Splash.this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
