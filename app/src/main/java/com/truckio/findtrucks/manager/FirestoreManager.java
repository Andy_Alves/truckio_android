package com.truckio.findtrucks.manager;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.Source;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.truckio.findtrucks.events.failures.UploadKYCFailureEvent;
import com.truckio.findtrucks.events.signin.PostRequestFailureEvent;
import com.truckio.findtrucks.events.success.AccountSettingsSaveSuccessEvent;
import com.truckio.findtrucks.events.success.FetchShipperSuccessEvent;
import com.truckio.findtrucks.events.success.PostRequestSuccessEvent;
import com.truckio.findtrucks.events.TruckRequestResultEvent;
import com.truckio.findtrucks.events.UpdateRequestList;
import com.truckio.findtrucks.events.signin.AccountCreatedResultEvent;
import com.truckio.findtrucks.events.success.UpdateUserInfoSuccessEvent;
import com.truckio.findtrucks.events.success.UploadKYCSucessEvent;
import com.truckio.findtrucks.model.KycShipper;
import com.truckio.findtrucks.model.Shipper;
import com.truckio.findtrucks.model.TruckRequest;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.EventBusException;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by Admin on 28,August,2019
 */
public class FirestoreManager {

    public static final String TAG = "FirestoreManager";

    public static final String DB_USER = "user_shippers";
    public static final String DB_KYC = "kyc_shipper";
    public static final String DB_TRUCK_REQUESTS = "truck_requests";

    public static final String STORAGE_KYC = "kyc_shipper";

    private static FirestoreManager ourInstance;

    public String userId;
    public String userPhone;
    public Shipper shipper;
    public List<TruckRequest> truckRequests;

    FirebaseFirestore firestore;

    public static FirestoreManager getInstance() {
        return ourInstance;
    }

    private FirestoreManager(Context context) {
        firestore =  FirebaseFirestore.getInstance();
        FirebaseStorage storage = FirebaseStorage.getInstance();

        userId = null;
        userPhone = null;
    }

    public static void intialize(Context context) {
        ourInstance = new FirestoreManager(context);
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setTruckRequests(List<TruckRequest> truckRequests) {
        this.truckRequests = truckRequests;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public Shipper getShipper() {
        Shipper shipper;
        shipper = this.shipper;

        return shipper;
    }

    public void signUpUser(Context context, Shipper shipper) {

        firestore.collection(DB_USER).document(shipper.getUser_id())
                .set(shipper)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {

                        FirestoreManager.getInstance().shipper = shipper;
                        SharedPrefsManager.saveUserId(context, shipper.user_id);
                        EventBus.getDefault().post(new AccountCreatedResultEvent(true));

                        Log.d(TAG, "DocumentSnapshot : Account successfully written!");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        EventBus.getDefault().post(new AccountCreatedResultEvent(false));
                        Log.w(TAG, "Error Creating document", e);
                    }
                });
    }

    public void fetchShipper(String userId) {

        Source source = Source.DEFAULT;

        DocumentReference docRef = firestore.collection(DB_USER).document(userId);
        docRef.get(source).addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                shipper = documentSnapshot.toObject(Shipper.class);

                if (shipper == null) {
                    Log.e(TAG, "onSuccess: No data found on Catch or database");
                } else {
                    EventBus.getDefault().post(new FetchShipperSuccessEvent());
                    Log.e(TAG, "onSuccess: shipper name : " + shipper.name);
                }
            }
        });

    }

    public void fetchRequests(String userId) {

        Source source = Source.DEFAULT;

        List<TruckRequest> requests = new ArrayList<>();

        Query docRef = firestore.collection(DB_TRUCK_REQUESTS)
                .whereEqualTo("user_id", userId);

        docRef.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {

                if (e != null) {
                    Log.w(TAG, "Listen failed.", e);
                    return;
                }

                if (queryDocumentSnapshots == null) {
                    Log.e(TAG, "Query snapshot null : " + requests);
                } else {
                    EventBus.getDefault().post(new UpdateRequestList());
                }

                for (DocumentChange dc : queryDocumentSnapshots.getDocumentChanges()) {
                    switch (dc.getType()) {
                        case ADDED:
                            Log.d(TAG, "New Request: " + dc.getDocument().getData());
                            break;
                        case MODIFIED:
                            Log.d(TAG, "Modified Request: " + dc.getDocument().getData());
                            break;
                        case REMOVED:
                            Log.d(TAG, "Removed Request: " + dc.getDocument().getData());
                            break;
                    }
                }

                List<TruckRequest> requests = new ArrayList<>();
                for (QueryDocumentSnapshot doc : queryDocumentSnapshots) {
                    requests.add(doc.toObject(TruckRequest.class));
                }

                if (requests.size() > 0) {

                }

                // TODO update specific data modification not querying all items

            }
        });

        docRef.get(source).addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {

                    for (QueryDocumentSnapshot document : task.getResult()) {
                        requests.add(document.toObject(TruckRequest.class));
                    }

                    Log.d(TAG, "onComplete() called with: task = [" + task + "]");

                    EventBus.getDefault().post(new TruckRequestResultEvent(requests));

                } else {
                    if (task.getException() != null) {
                        task.getException().printStackTrace();
                    }
                }
            }
        });
    }

    ///////////////////////
    //// POST TRUCK REQUEST
    ///////////////////////

    public void postTruckRequest(Context context, TruckRequest requests) {

        String timeStamp = String.valueOf(TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()));
        requests.setRequest_created_timestamp(timeStamp);

        firestore.collection(DB_TRUCK_REQUESTS).document(requests.request_id)
                .set(requests)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        EventBus.getDefault().post(new PostRequestSuccessEvent());
                        Log.d(TAG, "DocumentSnapshot : Account successfully written!");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        EventBus.getDefault().post(new PostRequestFailureEvent());
                        Log.w(TAG, "Error Creating document", e);
                    }
                });


    }

    public void deleteTruckRequest(TruckRequest requests) {

        firestore.collection(DB_TRUCK_REQUESTS).document(requests.request_id)
                .delete()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d(TAG, "DocumentSnapshot : Account successfully deleted!");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error Creating document", e);
                    }
                });
    }

    /////////////////////////
    ///////// KYC
    /////////////////////////

    public void uploadKYC(KycShipper kycShipper, String[] imgUri) {

        StorageReference storageReference = FirebaseStorage.getInstance().getReference();

        for (int i = 0; i < imgUri.length; i++) {

            int pos = i;

            File file = new File(imgUri[i]);
            Uri uriUpload = Uri.fromFile(file);

            StorageReference imgRef = storageReference.child(STORAGE_KYC).child(kycShipper.userId).child(file.getName());

            imgRef.putFile(uriUpload).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    if (pos == 3) {
                        EventBus.getDefault().post(new UploadKYCSucessEvent());
                    }
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    EventBus.getDefault().post(new UploadKYCFailureEvent());
                }
            });
        }

        DocumentReference kycReference = firestore.collection(DB_USER).document(userId).collection(DB_KYC).document();

        kycReference.set(kycShipper).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.d(TAG, "onSuccess() called with: aVoid = [" + aVoid + "]");
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d(TAG, "onFailure() called with: e = [" + e + "]");
            }
        });

    }



    //////////////////////////////
    /////// REAL TIME FIRESTORE UPDATES
    /////////////////////////////

    public void updateShipperInfo(Shipper shipper) {

        DocumentReference docRef = firestore.collection(DB_USER).document(userId);
        docRef.set(shipper).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                EventBus.getDefault().post(new AccountSettingsSaveSuccessEvent());
            }
        });

    }

}
