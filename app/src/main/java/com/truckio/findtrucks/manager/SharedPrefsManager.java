package com.truckio.findtrucks.manager;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by Andrews on 29,August,2019
 */
public class SharedPrefsManager {

    static final String PREF_SHIPPER_ID = "user_id";

    static SharedPreferences getSharedPreferences(Context ctx) {
        return PreferenceManager.getDefaultSharedPreferences(ctx);
    }

    public static String getShipperId(Context context) {
        return getSharedPreferences(context).getString(PREF_SHIPPER_ID, null);
    }

    public static void saveUserId(Context context, String userId) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(PREF_SHIPPER_ID, userId);
        editor.apply();
    }


}
