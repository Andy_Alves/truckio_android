package com.truckio.findtrucks;

import java.util.Calendar;

/**
 * Created by Andrews on 20,August,2019
 */

public class Utils {

    public static final String OPEN = "OPEN";
    public static final String CONTAINER = "CONTAINER";
    public static final String TRAILER = "TRAILER";

    public static final String FOOD = "FOOD";
    public static final String ELECTRONICS = "ELECTRONICS";
    public static final String FMCG = "FMCG";
    public static final String MEDICINE = "MEDICINE";
    public static final String BUILDING_MATERIALS = "BUILDING_MATERIALS";
    public static final String SPARES = "SPARES";

    public static String getMonthString(int month) {
        switch (month) {
            case 1:
                return "JAN";
            case 2:
                return "FEB";
            case 3:
                return "MAR";
            case 4:
                return "APR";
            case 5:
                return "MAY";
            case 6:
                return "JUNE";
            case 7:
                return "JULY";
            case 8:
                return "AUG";
            case 9:
                return "SEPT";
            case 10:
                return "OCT";
            case 11:
                return "NOV";
            case 12:
                return "DEC";

                default:
                    return "   ";
        }
    }



}
