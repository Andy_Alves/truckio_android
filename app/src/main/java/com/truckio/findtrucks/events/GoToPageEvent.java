package com.truckio.findtrucks.events;

import com.truckio.findtrucks.enums.MainTabs;

/**
 * Created by Admin on 11,August,2019
 */
public class GoToPageEvent {
    public MainTabs mainTabs;

    public GoToPageEvent(MainTabs mainTabs) {
        this.mainTabs = mainTabs;
    }
}
