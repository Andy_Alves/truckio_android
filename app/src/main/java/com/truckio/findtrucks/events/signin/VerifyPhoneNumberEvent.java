package com.truckio.findtrucks.events.signin;

import com.google.firebase.auth.PhoneAuthCredential;

/**
 * Created by Andrews on 26,August,2019
 */
public class VerifyPhoneNumberEvent {

    public String phonenumber;

    public VerifyPhoneNumberEvent(String phonenumber) {
        this.phonenumber = phonenumber;
    }
}
