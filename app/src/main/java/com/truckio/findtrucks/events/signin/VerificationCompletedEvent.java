package com.truckio.findtrucks.events.signin;

import com.google.firebase.auth.PhoneAuthCredential;

/**
 * Created by Andrews on 27,August,2019
 */
public class VerificationCompletedEvent {

    public PhoneAuthCredential phoneAuthCredential;

    public VerificationCompletedEvent(PhoneAuthCredential phoneAuthCredential) {
        this.phoneAuthCredential = phoneAuthCredential;
    }
}
