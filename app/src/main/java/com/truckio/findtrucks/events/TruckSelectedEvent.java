package com.truckio.findtrucks.events;

import com.truckio.findtrucks.enums.TruckType;

/**
 * Created by Admin on 19,August,2019
 */
public class TruckSelectedEvent {
    public String tons;
    public TruckType truckType;

    public TruckSelectedEvent(String tons, TruckType truckType) {
        this.tons = tons;
        this.truckType = truckType;
    }
}
