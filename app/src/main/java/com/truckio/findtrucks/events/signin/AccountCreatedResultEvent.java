package com.truckio.findtrucks.events.signin;

/**
 * Created by Admin on 28,August,2019
 */
public class AccountCreatedResultEvent {
    public boolean isSuccess;

    public AccountCreatedResultEvent(boolean isSuccess) {
        this.isSuccess = isSuccess;
    }
}
