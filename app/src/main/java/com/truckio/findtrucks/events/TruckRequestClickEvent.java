package com.truckio.findtrucks.events;

import com.truckio.findtrucks.model.TruckRequest;

/**
 * Created by Admin on 03,September,2019
 */
public class TruckRequestClickEvent {

    public TruckRequest requests;
    public int position;

    public TruckRequestClickEvent(TruckRequest requests, int position) {
        this.requests = requests;
        this.position = position;
    }
}
