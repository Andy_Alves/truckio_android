package com.truckio.findtrucks.events;

import com.truckio.findtrucks.model.TruckRequest;

import java.util.List;

/**
 * Created by Admin on 02,September,2019
 */
public class TruckRequestResultEvent {
    public List<TruckRequest> truckRequest;

    public TruckRequestResultEvent(List truckRequest) {
        this.truckRequest = truckRequest;
    }

}
