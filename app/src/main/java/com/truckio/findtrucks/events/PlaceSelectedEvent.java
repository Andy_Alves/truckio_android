package com.truckio.findtrucks.events;

import com.google.android.libraries.places.api.model.AutocompletePrediction;

/**
 * Created by Andrews on 15,August,2019
 */

public class PlaceSelectedEvent {

    public AutocompletePrediction place;

    public PlaceSelectedEvent(AutocompletePrediction place) {
        this.place = place;
    }

}
