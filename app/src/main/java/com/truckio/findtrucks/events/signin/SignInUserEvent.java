package com.truckio.findtrucks.events.signin;

import com.google.firebase.auth.PhoneAuthCredential;

/**
 * Created by Andrews on 27,August,2019
 */
public class SignInUserEvent {
    public PhoneAuthCredential credential;

    public SignInUserEvent(PhoneAuthCredential credential) {
        this.credential = credential;
    }
}
