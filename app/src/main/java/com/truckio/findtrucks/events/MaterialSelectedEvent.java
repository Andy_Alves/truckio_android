package com.truckio.findtrucks.events;

import com.truckio.findtrucks.enums.MaterialType;

/**
 * Created by Andrews on 19,August,2019
 */
public class MaterialSelectedEvent {
    public MaterialType materialType;

    public MaterialSelectedEvent(MaterialType materialType) {
        this.materialType = materialType;
    }
}
