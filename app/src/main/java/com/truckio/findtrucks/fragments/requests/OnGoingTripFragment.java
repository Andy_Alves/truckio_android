package com.truckio.findtrucks.fragments.requests;


import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import com.truckio.findtrucks.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class OnGoingTripFragment extends Fragment {


    @BindView(R.id.cl_empty_ongoing_list)
    ConstraintLayout clEmptyOnGoingTrips;

    public List requestList;

    public OnGoingTripFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_on_going_trip, container, false);
        ButterKnife.bind(this,v);

        clEmptyOnGoingTrips.setVisibility(View.GONE);
        if(requestList == null) {
            clEmptyOnGoingTrips.setVisibility(View.VISIBLE);
        } else if (requestList.isEmpty()) {
            clEmptyOnGoingTrips.setVisibility(View.VISIBLE);
        }

        // Inflate the layout for this fragment
        return v;
    }

}
