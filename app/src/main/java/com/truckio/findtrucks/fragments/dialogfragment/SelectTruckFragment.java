package com.truckio.findtrucks.fragments.dialogfragment;



import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.andrefrsousa.superbottomsheet.SuperBottomSheetFragment;
import com.bruce.pickerview.LoopScrollListener;
import com.bruce.pickerview.LoopView;
import com.truckio.findtrucks.R;
import com.truckio.findtrucks.enums.TruckType;
import com.truckio.findtrucks.events.TruckSelectedEvent;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class SelectTruckFragment extends SuperBottomSheetFragment {

    @BindView(R.id.loop_view)
    LoopView tonloopView;

    @BindView(R.id.tv_open_truck)
    TextView tvOpenTruck;

    @BindView(R.id.tv_container_truck)
    TextView tvContainerTruck;

    @BindView(R.id.tv_trailer_truck)
    TextView tvTrailerTruck;

    @BindView(R.id.btn_truck_open)
    ImageView imgOpenTruck;

    @BindView(R.id.btn_truck_container)
    ImageView imgContainerTruck;

    @BindView(R.id.btn_truck_trailer)
    ImageView imgTrailerTruck;

    public ArrayList<String> weightList;

    String tons;
    TruckType truckType;

    public static SelectTruckFragment newInstance(String truckType, String tons) {
        SelectTruckFragment fragment = new SelectTruckFragment();

        if(truckType == null) {
            return fragment;
        }

        Bundle args = new Bundle();
        args.putString("truck_type", truckType);
        args.putString("tons",tons);
        fragment.setArguments(args);

        return fragment;
    }

    public SelectTruckFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View v = inflater.inflate(R.layout.fragment_select_truck, container, false);
        ButterKnife.bind(this,v);

        addWeightList();
        tonloopView.setDataList(weightList);
        tonloopView.setLoopListener(new LoopScrollListener() {
            @Override
            public void onItemSelect(int item) {
                tons = weightList.get(item);
            }
        });

        if (getArguments() != null) {
            tons = getArguments().getString("tons");
            truckType = TruckType.valueOf(getArguments().getString("truck_type"));
            updateView(truckType);
            tonloopView.setInitPosition(weightList.indexOf(tons));
        }

        setCancelable(false);

        return v;
    }

    @Override
    public int getPeekHeight() {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 320, getResources().getDisplayMetrics());
    }

    public void addWeightList() {
        weightList = new ArrayList<>();

        float ton = 1.0f;
        for(int i = 1 ; i <= 70 ; i++) {
            weightList.add(String.valueOf(ton));
            ton += 0.5f;
        }

        tons = weightList.get(3);
    }

    @OnClick(R.id.btn_done__select_truck)
    public void clickDone() {
        if (truckType != null) {
            EventBus.getDefault().post(new TruckSelectedEvent(tons, truckType));
        }
        dismiss();
    }

    @OnClick(R.id.btn_truck_open)
    public void clickOpen() {
        updateView(TruckType.OPEN);
        truckType = TruckType.OPEN;
    }

    @OnClick(R.id.btn_truck_container)
    public void clickContainer() {
        updateView(TruckType.CONTAINER);
        truckType = TruckType.CONTAINER;
    }

    @OnClick(R.id.btn_truck_trailer)
    public void clickTrailer() {
        updateView(TruckType.TRAILER);
        truckType = TruckType.TRAILER;
    }

    public void updateView(TruckType truckType) {

        imgOpenTruck.setImageResource(R.drawable.ic_truck_open);
        imgContainerTruck.setImageResource(R.drawable.ic_container);
        imgTrailerTruck.setImageResource(R.drawable.ic_truck_trailer);
        tvOpenTruck.setTextColor(getResources().getColor(R.color.greyish_brown));
        tvContainerTruck.setTextColor(getResources().getColor(R.color.greyish_brown));
        tvTrailerTruck.setTextColor(getResources().getColor(R.color.greyish_brown));

        switch (truckType) {
            case OPEN:
                imgOpenTruck.setImageResource(R.drawable.ic_truck_open_selected);
                tvOpenTruck.setTextColor(getResources().getColor(R.color.truckio_orange));
                break;
            case CONTAINER:
                imgContainerTruck.setImageResource(R.drawable.ic_container_selected);
                tvContainerTruck.setTextColor(getResources().getColor(R.color.truckio_orange));
                break;
            case TRAILER:
                imgTrailerTruck.setImageResource(R.drawable.ic_truck_trailer_selected);
                tvTrailerTruck.setTextColor(getResources().getColor(R.color.truckio_orange));
                break;
        }
    }


}

