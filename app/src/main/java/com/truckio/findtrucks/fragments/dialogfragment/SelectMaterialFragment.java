package com.truckio.findtrucks.fragments.dialogfragment;

import android.content.res.Resources;
import android.os.Bundle;


import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.andrefrsousa.superbottomsheet.SuperBottomSheetFragment;
import com.truckio.findtrucks.R;
import com.truckio.findtrucks.enums.MaterialType;
import com.truckio.findtrucks.events.MaterialSelectedEvent;

import org.greenrobot.eventbus.EventBus;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class SelectMaterialFragment extends SuperBottomSheetFragment {

    public static SelectMaterialFragment newInstance() {
        return new SelectMaterialFragment();
    }

    public SelectMaterialFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_select_material, container, false);
        ButterKnife.bind(this,view);

        return view;
    }

    @Override
    public int getPeekHeight() {
        return (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 320, getResources().getDisplayMetrics());
    }

    @OnClick(R.id.ll_btn_food)
    public void click1() {
        selectedMateiral(MaterialType.FOOD);
    }

    @OnClick(R.id.ll_btn_electornics)
    public void click2() {
        selectedMateiral(MaterialType.ELECTRONICS);
    }

    @OnClick(R.id.ll_btn_fmcg)
    public void click3() {
        selectedMateiral(MaterialType.FMCG);
    }

    @OnClick(R.id.ll_btn_medicine)
    public void click4() {
        selectedMateiral(MaterialType.MEDICINE);
    }

    @OnClick(R.id.ll_btn_building_material)
    public void click5() {
        selectedMateiral(MaterialType.BUILDING_MATERIALS);
    }

    @OnClick(R.id.ll_btn_spares)
    public void click6() {
        selectedMateiral(MaterialType.SPARES);
    }

    public void selectedMateiral(MaterialType materialType) {
        EventBus.getDefault().post(new MaterialSelectedEvent(materialType));
        dismiss();
    }
}
