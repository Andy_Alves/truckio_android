package com.truckio.findtrucks.fragments;


import android.content.Intent;
import android.os.Bundle;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.truckio.findtrucks.R;
import com.truckio.findtrucks.activity.AccountSettingsActivity;
import com.truckio.findtrucks.activity.KYCActivity;
import com.truckio.findtrucks.events.LogoutEvent;
import com.truckio.findtrucks.events.success.AccountSettingsSaveSuccessEvent;
import com.truckio.findtrucks.events.success.UpdateUserInfoSuccessEvent;
import com.truckio.findtrucks.manager.FirestoreManager;
import com.truckio.findtrucks.manager.SharedPrefsManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.w3c.dom.Text;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class AccountFragment extends Fragment {

    @BindView(R.id.tv_account_name)
    TextView tvName;

    @BindView(R.id.tv_account_phno)
    TextView tvPhone;

    public AccountFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_account, container, false);
        ButterKnife.bind(this, view);
        EventBus.getDefault().register(this);

        updateView();
        return view;
    }

    public void updateView() {

        if(FirestoreManager.getInstance().shipper == null) {
            Log.e("Account Fragment", "updateView: shipper null");
            return;
        }

        tvName.setText(FirestoreManager.getInstance().shipper.name);
        tvPhone.setText(FirestoreManager.getInstance().shipper.phone);
    }

    @Override
    public void onDestroyView() {
        EventBus.getDefault().unregister(this);
        super.onDestroyView();
    }

    @OnClick(R.id.img_btn_account_settings)
    public void clickAccountSettings() {
        Intent intent = new Intent(getContext(), AccountSettingsActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.btn_cl_kyc_details)
    public void clickKYC() {
        Intent intent = new Intent(getContext(), KYCActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.btn_cl_logout)
    public void clickLogOut() {
        EventBus.getDefault().post(new LogoutEvent());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(AccountSettingsSaveSuccessEvent event) {
        Toast.makeText(getContext(), "Saved Changes", Toast.LENGTH_SHORT).show();
        updateView();
    }

}
