package com.truckio.findtrucks.fragments;


import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.truckio.findtrucks.R;
import com.truckio.findtrucks.adapters.MainViewPagerAdapter;
import com.truckio.findtrucks.adapters.RequestsViewPagerAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class RequestFragment extends Fragment {

    @BindView(R.id.viewpager_requests_trips)
    ViewPager viewPager;

    @BindView(R.id.btn_requests_list)
    TextView tvBtnRequests;

    @BindView(R.id.btn_ongoingtrips)
    TextView tvBtnOnGoingTrips;

    private RequestsViewPagerAdapter viewPagerAdapter;

    public RequestFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_request, container, false);
        ButterKnife.bind(this, v);

        viewPagerAdapter = new RequestsViewPagerAdapter(getChildFragmentManager());
        viewPager.setAdapter(viewPagerAdapter);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {

                tvBtnRequests.setTextColor(getResources().getColor(R.color.greyish_brown));
                tvBtnOnGoingTrips.setTextColor(getResources().getColor(R.color.greyish_brown));

                switch (i) {
                    case 0:
                        tvBtnRequests.setTextColor(getResources().getColor(R.color.truckio_orange));
                        break;
                    case 1:
                        tvBtnOnGoingTrips.setTextColor(getResources().getColor(R.color.truckio_orange));
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

        clickRequest();

        // Inflate the layout for this fragment
        return v;
    }

    @OnClick(R.id.btn_requests_list)
    public void clickRequest() {
        tvBtnRequests.setTextColor(getResources().getColor(R.color.truckio_orange));
        tvBtnOnGoingTrips.setTextColor(getResources().getColor(R.color.greyish_brown));
        viewPager.setCurrentItem(0);
    }

    @OnClick(R.id.btn_ongoingtrips)
    public void clickOnGoingTrips() {
        tvBtnRequests.setTextColor(getResources().getColor(R.color.greyish_brown));
        tvBtnOnGoingTrips.setTextColor(getResources().getColor(R.color.truckio_orange));
        viewPager.setCurrentItem(1);
    }

}
