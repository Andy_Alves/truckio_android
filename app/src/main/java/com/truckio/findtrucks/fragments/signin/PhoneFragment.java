package com.truckio.findtrucks.fragments.signin;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.FirebaseException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.truckio.findtrucks.R;
import com.truckio.findtrucks.events.signin.PhoneErrorEvent;
import com.truckio.findtrucks.events.signin.VerifyPhoneNumberEvent;
import com.truckio.findtrucks.views.IndeterminantProgressBar;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.w3c.dom.Text;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import studio.carbonylgroup.textfieldboxes.ExtendedEditText;

/**
 * A simple {@link Fragment} subclass.
 */
public class PhoneFragment extends Fragment {

    public static final String TAG = "PhoneFragment";

    public static final String COUNTRY_CODE = "+91";

    @BindView(R.id.extented_et_phone)
    ExtendedEditText editTextPhone;

    @BindView(R.id.progress_circular__phone)
    IndeterminantProgressBar progressBar;

    @BindView(R.id.tv_next)
    TextView tvNext;

    public PhoneFragment() {
        // Required empty public constructor
    }

    public static Fragment newInstance() {
        PhoneFragment phoneFragment = new PhoneFragment();
        return phoneFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_phone, container, false);
        ButterKnife.bind(this, view);
        EventBus.getDefault().register(this);

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);
    }

    @OnClick(R.id.btn_next__phone)
    public void clickNext() {

        if (!validate()) {
            return;
        }

        progressBar.setVisibility(View.VISIBLE);
        tvNext.setVisibility(View.GONE);
        EventBus.getDefault().post(new VerifyPhoneNumberEvent(editTextPhone.getText().toString()));
    }

    public boolean validate() {
        if (editTextPhone.getText().length() > 10 || editTextPhone.getText().length() < 10) {
            editTextPhone.setError("Number must be 10 digit");
            return false;
        }

        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(PhoneErrorEvent event) {
        progressBar.setVisibility(View.GONE);
        tvNext.setVisibility(View.VISIBLE);
        Toast.makeText(getContext(), event.error, Toast.LENGTH_SHORT).show();
    }
}
