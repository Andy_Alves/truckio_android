package com.truckio.findtrucks.fragments.dialogfragment;


import android.content.Intent;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.andrefrsousa.superbottomsheet.SuperBottomSheetFragment;
import com.truckio.findtrucks.R;
import com.truckio.findtrucks.Utils;
import com.truckio.findtrucks.activity.EditRequestActivity;
import com.truckio.findtrucks.enums.MaterialType;
import com.truckio.findtrucks.enums.TruckType;
import com.truckio.findtrucks.model.TruckRequest;

import java.text.DecimalFormat;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class PreviewRequestFragment extends SuperBottomSheetFragment {

    @BindView(R.id.ll_requestfragment_rootparentview)
    LinearLayout rootView;

    @BindView(R.id.tv_pickpoint)
    TextView tvPickpoint;

    @BindView(R.id.tv_droppoint1)
    TextView tvDroppoint1;

    @BindView(R.id.ll_droppoint2_view)
    LinearLayout droppoint2View;
    @BindView(R.id.tv_droppoint2)
    TextView tvDroppoint2;

    @BindView(R.id.ll_droppoint3_view)
    LinearLayout droppoint3View;
    @BindView(R.id.tv_droppoint3)
    TextView tvDroppoint3;

    @BindView(R.id.tv_truckname)
    TextView tvTruckName;
    @BindView(R.id.iv_ic_trucktype)
    ImageView ivTruck;

    @BindView(R.id.tv_materialname)
    TextView tvMateiralName;
    @BindView(R.id.iv_ic_materialtype)
    ImageView ivMaterial;

    @BindView(R.id.tv_estimated_price)
    TextView tvEstimatedPrice;

    @BindView(R.id.tv_pickuptime)
    TextView tvPickupTime;

    @BindView(R.id.btn_edit_request)
    ImageButton btnEditRequest;

    public TruckRequest requestModel;

    public int index;

    public static PreviewRequestFragment newInstance(TruckRequest requestModel, int position) {
        PreviewRequestFragment fragment = new PreviewRequestFragment();
        fragment.requestModel = requestModel;
        fragment.index = position;

        return fragment;
    }

    public PreviewRequestFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_preview_request, container, false);
        ButterKnife.bind(this, view);

        updateView();

        return view;
    }

    @Override
    public int getPeekHeight() {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 400, getResources().getDisplayMetrics());
    }

    public void updateView() {

        tvPickpoint.setText(requestModel.pickup_point);
        tvDroppoint1.setText(requestModel.drop_point1);

        if (requestModel.drop_point2 != null) {
            droppoint2View.setVisibility(View.VISIBLE);
            tvDroppoint2.setText(requestModel.drop_point2);
        }

        if (requestModel.drop_point3 != null) {
            droppoint3View.setVisibility(View.VISIBLE);
            tvDroppoint3.setText(requestModel.drop_point3);
        }

        switch (TruckType.valueOf(requestModel.truck_type)) {

            case OPEN:
                ivTruck.setImageResource(R.drawable.ic_truck_open);
                tvTruckName.setText(requestModel.tons + " Ton's" + " Open Body");
                break;

                case CONTAINER:
                    ivTruck.setImageResource(R.drawable.ic_container);
                    tvTruckName.setText(requestModel.tons + " Ton's" + " Container");
                    break;
                case TRAILER:
                    ivTruck.setImageResource(R.drawable.ic_truck_trailer);
                    tvTruckName.setText(requestModel.tons + " Ton's" + " Trailer");
                    break;
            }

        switch (MaterialType.valueOf(requestModel.material_type)) {

            case FOOD:
                ivMaterial.setImageResource(R.drawable.ic_food);
                tvMateiralName.setText("Food & Agri");
                break;

                case ELECTRONICS:
                    ivMaterial.setImageResource(R.drawable.ic_electronics);
                    tvMateiralName.setText("Electronics");
                    break;
                case FMCG:
                    ivMaterial.setImageResource(R.drawable.ic_fmcg);
                    tvMateiralName.setText("FMCG");
                    break;
                case MEDICINE:
                    ivMaterial.setImageResource(R.drawable.ic_medicine);
                    tvMateiralName.setText("Medicine");
                    break;
                case BUILDING_MATERIALS:
                    ivMaterial.setImageResource(R.drawable.ic_building_material);
                    tvMateiralName.setText("Building Material");
                    break;
                case SPARES:
                    ivMaterial.setImageResource(R.drawable.ic_spares);
                    tvMateiralName.setText("Spares");
                    break;
            }

        if (requestModel.pickuptime.get(0) != 0 && requestModel.pickuptime.get(1) != 0) {
            String month = Utils.getMonthString(requestModel.pickuptime.get(0) ) + " " + requestModel.pickuptime.get(1);

            int hours = requestModel.pickuptime.get(2);
            String meridien = " am";

            if (requestModel.pickuptime.get(2) > 12) {
                hours = requestModel.pickuptime.get(2) - 12;
                meridien = " pm";
            }

            // format to two decimal
            String hour =  new DecimalFormat("00").format(hours);
            String min =  new DecimalFormat("00").format(requestModel.pickuptime.get(3));
            String time = hour + ":" + min + meridien;

            String pickUpTime = month + " " + time;
            tvPickupTime.setText(pickUpTime);

        }
    }

    @OnClick(R.id.btn_edit_request)
    public void clickEdit() {
        Intent intent = new Intent(getActivity(), EditRequestActivity.class);
        intent.putExtra(EditRequestActivity.KEY_EDITING_REQUEST, index);
        startActivity(intent);
        dismiss();
    }

    @OnClick(R.id.btn_book_truck)
    public void clickBookRequest() {
        dismiss();
    }

}
