package com.truckio.findtrucks.fragments.dialogfragment;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.andrefrsousa.superbottomsheet.SuperBottomSheetFragment;
import com.truckio.findtrucks.R;
import com.truckio.findtrucks.Utils;
import com.truckio.findtrucks.enums.MaterialType;
import com.truckio.findtrucks.enums.TruckType;
import com.truckio.findtrucks.events.signin.PostRequestFailureEvent;
import com.truckio.findtrucks.events.success.PostRequestSuccessEvent;
import com.truckio.findtrucks.manager.FirestoreManager;
import com.truckio.findtrucks.model.TruckRequest;
import com.truckio.findtrucks.views.IndeterminantProgressBar;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.DecimalFormat;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class RequestTruckFragment extends SuperBottomSheetFragment {

    @BindView(R.id.ll_requestfragment_rootparentview)
    LinearLayout rootView;

    @BindView(R.id.tv_pickpoint)
    TextView tvPickpoint;

    @BindView(R.id.tv_droppoint1)
    TextView tvDroppoint1;

    @BindView(R.id.ll_droppoint2_view)
    LinearLayout droppoint2View;
    @BindView(R.id.tv_droppoint2)
    TextView tvDroppoint2;

    @BindView(R.id.ll_droppoint3_view)
    LinearLayout droppoint3View;
    @BindView(R.id.tv_droppoint3)
    TextView tvDroppoint3;

    @BindView(R.id.tv_truckname)
    TextView tvTruckName;
    @BindView(R.id.iv_ic_trucktype)
    ImageView ivTruck;

    @BindView(R.id.tv_materialname)
    TextView tvMateiralName;
    @BindView(R.id.iv_ic_materialtype)
    ImageView ivMaterial;

    @BindView(R.id.tv_estimated_price)
    TextView tvEstimatedPrice;

    @BindView(R.id.tv_pickuptime)
    TextView tvPickupTime;

    public TruckRequest requestModel;


    @BindView(R.id.progress_bar_truck_requests)
    IndeterminantProgressBar progressBar;

    @BindView(R.id.btn_request_text)
    TextView tvRequest;

    public static RequestTruckFragment newInstance(TruckRequest requestModel) {
        RequestTruckFragment fragment = new RequestTruckFragment();
        fragment.requestModel = requestModel;

        return fragment;
    }



    public RequestTruckFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_request_truck, container, false);
        ButterKnife.bind(this, view);
        EventBus.getDefault().register(this);


        updateView();

        return view;
    }

    @Override
    public void onDestroyView() {
        EventBus.getDefault().unregister(this);
        super.onDestroyView();
    }

    @Override
    public int getPeekHeight() {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 400, getResources().getDisplayMetrics());
    }

    public void updateView() {

        tvPickpoint.setText(requestModel.pickup_point);
        tvDroppoint1.setText(requestModel.drop_point1);

        if (requestModel.drop_point2 != null) {
            droppoint2View.setVisibility(View.VISIBLE);
            tvDroppoint2.setText(requestModel.drop_point2);
        }

        if (requestModel.drop_point3 != null) {
            droppoint3View.setVisibility(View.VISIBLE);
            tvDroppoint3.setText(requestModel.drop_point3);
        }

        TruckType truckType = TruckType.valueOf(requestModel.truck_type);
        switch (truckType) {

            case OPEN:
                ivTruck.setImageResource(R.drawable.ic_truck_open);
                tvTruckName.setText(requestModel.tons + " Ton's" + " Open Body");
                break;

                case CONTAINER:
                    ivTruck.setImageResource(R.drawable.ic_container);
                    tvTruckName.setText(requestModel.tons + " Ton's" + " Container");
                    break;
                case TRAILER:
                    ivTruck.setImageResource(R.drawable.ic_truck_trailer);
                    tvTruckName.setText(requestModel.tons + " Ton's" + " Trailer");
                    break;
            }

        MaterialType materialType = MaterialType.valueOf(requestModel.material_type);
        switch (materialType) {

            case FOOD:
                ivMaterial.setImageResource(R.drawable.ic_food);
                tvMateiralName.setText("Food & Agri");
                break;

                case ELECTRONICS:
                    ivMaterial.setImageResource(R.drawable.ic_electronics);
                    tvMateiralName.setText("Electronics");
                    break;
                case FMCG:
                    ivMaterial.setImageResource(R.drawable.ic_fmcg);
                    tvMateiralName.setText("FMCG");
                    break;
                case MEDICINE:
                    ivMaterial.setImageResource(R.drawable.ic_medicine);
                    tvMateiralName.setText("Medicine");
                    break;
                case BUILDING_MATERIALS:
                    ivMaterial.setImageResource(R.drawable.ic_building_material);
                    tvMateiralName.setText("Building Material");
                    break;
                case SPARES:
                    ivMaterial.setImageResource(R.drawable.ic_spares);
                    tvMateiralName.setText("Spares");
                    break;
            }


        if (requestModel.getMonth() != 0 && requestModel.getDate() != 0) {
            String month = Utils.getMonthString(requestModel.getMonth() ) + " " + requestModel.getDate();

            int hours = requestModel.getHours();
            String meridien = " am";

            if (requestModel.getHours() > 12) {
                hours = requestModel.getHours() - 12;
                meridien = " pm";
            }

            // format to two decimal
            String hour =  new DecimalFormat("00").format(hours);
            String min =  new DecimalFormat("00").format(requestModel.getMinutes());
            String time = hour + ":" + min + meridien;

            String pickUpTime = month + " " + time;
            tvPickupTime.setText(pickUpTime);

        }
    }

    @OnClick(R.id.btn_request)
    public void clickRequest() {
        progressBar.setVisibility(View.VISIBLE);
        tvRequest.setVisibility(View.GONE);
        FirestoreManager.getInstance().postTruckRequest(getContext(), requestModel);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(PostRequestSuccessEvent event) {
        progressBar.setVisibility(View.VISIBLE);
        tvRequest.setVisibility(View.GONE);
        dismiss();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(PostRequestFailureEvent event) {
        progressBar.setVisibility(View.GONE);
        tvRequest.setVisibility(View.VISIBLE);
        Toast.makeText(getContext(), "Something went wrong!", Toast.LENGTH_SHORT).show();
    }

}
