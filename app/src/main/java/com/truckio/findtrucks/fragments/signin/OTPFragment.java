package com.truckio.findtrucks.fragments.signin;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.mukesh.OtpView;
import com.truckio.findtrucks.R;
import com.truckio.findtrucks.events.signin.OTPErrorEvent;
import com.truckio.findtrucks.events.signin.PhoneErrorEvent;
import com.truckio.findtrucks.events.signin.SignInUserEvent;
import com.truckio.findtrucks.events.signin.VerificationCompletedEvent;
import com.truckio.findtrucks.views.IndeterminantProgressBar;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class OTPFragment extends Fragment {

    public static final String TAG = "OTPFragment";

    @BindView(R.id.otp_view)
    OtpView otpView;

    @BindView(R.id.progress_circular__phone)
    IndeterminantProgressBar progressBar;

    @BindView(R.id.tv_next)
    TextView tvNext;

    String verificationId;
    PhoneAuthCredential credential;
    private FirebaseAuth mAuth;

    public OTPFragment() {
        // Required empty public constructor
    }

    public static OTPFragment newInstance(String verificationId) {
        OTPFragment otpFragment = new OTPFragment();

        Bundle bundle = new Bundle();
        bundle.putString("verificationId", verificationId);
        otpFragment.setArguments(bundle);

        return otpFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_otp, container, false);
        ButterKnife.bind(this, view);
        EventBus.getDefault().register(this);

        if (getArguments() != null) {
            verificationId = getArguments().getString("verificationId");
        }

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);
    }

    @OnClick(R.id.btn_next__otp)
    public void clickSignIn() {
        progressBar.setVisibility(View.VISIBLE);
        tvNext.setVisibility(View.GONE);
        credential = PhoneAuthProvider.getCredential(verificationId, otpView.getText().toString());
        EventBus.getDefault().post(new SignInUserEvent(credential));
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(OTPErrorEvent event) {
        progressBar.setVisibility(View.GONE);
        tvNext.setVisibility(View.VISIBLE);
        Toast.makeText(getContext(), event.error, Toast.LENGTH_SHORT).show();
    }
}
