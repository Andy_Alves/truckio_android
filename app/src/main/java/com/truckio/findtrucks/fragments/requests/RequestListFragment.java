package com.truckio.findtrucks.fragments.requests;


import android.os.Bundle;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.truckio.findtrucks.R;
import com.truckio.findtrucks.adapters.TruckRequestsRecyclerAdapter;
import com.truckio.findtrucks.enums.MainTabs;
import com.truckio.findtrucks.events.GoToPageEvent;
import com.truckio.findtrucks.events.TruckRequestClickEvent;
import com.truckio.findtrucks.events.TruckRequestResultEvent;
import com.truckio.findtrucks.events.UpdateRequestList;
import com.truckio.findtrucks.fragments.dialogfragment.PreviewRequestFragment;
import com.truckio.findtrucks.manager.FirestoreManager;
import com.truckio.findtrucks.model.TruckRequest;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */

public class RequestListFragment extends Fragment {

    @BindView(R.id.cl_empty_list)
    ConstraintLayout clNoRequestFound;

    @BindView(R.id.btn_request_list)
    LinearLayout btnRequestList;

    @BindView(R.id.progress_bar_truck_requests)
    ProgressBar progressBar;

    @BindView(R.id.rv_requestlist)
    RecyclerView recyclerView;

    TruckRequestsRecyclerAdapter adapter ;

    public List<TruckRequest> requestList;

    private boolean isFetching = false;

    public RequestListFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_requestlist, container, false);
        ButterKnife.bind(this,v);
        EventBus.getDefault().register(this);

        fetchList(new UpdateRequestList());

        requestList = new ArrayList<>();
        adapter = new TruckRequestsRecyclerAdapter(getContext(), requestList);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);


        // Inflate the layout for this fragment
        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);
    }

    @OnClick(R.id.btn_request_list)
    public void clikcRequestList() {
        EventBus.getDefault().post(new GoToPageEvent(MainTabs.POST_TRUCK_TAB));
    }

    ///////////////////////////
    //////// EVENTS
    ///////////////////////////

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(TruckRequestClickEvent event) {
        PreviewRequestFragment.newInstance(event.requests, event.position).show(getActivity().getSupportFragmentManager(), "PreviewRequestFragment");
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void fetchList(UpdateRequestList event) {

        if (isFetching){
            return;
        }

        recyclerView.setVisibility(View.GONE);
        clNoRequestFound.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);

        FirestoreManager.getInstance().fetchRequests(FirestoreManager.getInstance().userId);
        isFetching = true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(TruckRequestResultEvent event) {

        adapter.updateList(event.truckRequest);
        FirestoreManager.getInstance().setTruckRequests(event.truckRequest);

        Log.d("truck reqeusts ", "onMessageEvent: truck request length : " + requestList.size());

        progressBar.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);

        if (requestList == null) {
            clNoRequestFound.setVisibility(View.VISIBLE);
        } else if (requestList.isEmpty()) {
            clNoRequestFound.setVisibility(View.VISIBLE);
        }

        isFetching = false;
    }




}
