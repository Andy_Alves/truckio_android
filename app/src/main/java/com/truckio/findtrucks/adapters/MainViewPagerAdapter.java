package com.truckio.findtrucks.adapters;


import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.truckio.findtrucks.fragments.AccountFragment;
import com.truckio.findtrucks.fragments.PostTruckFragment;
import com.truckio.findtrucks.fragments.RequestFragment;

/**
 * Created by Andrews on 10,August,2019
 */

public class MainViewPagerAdapter extends FragmentPagerAdapter {

    public MainViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int i) {

        switch (i) {
            case 0:
                return new RequestFragment();
            case 1:
                return new PostTruckFragment();
            case 2:
                return new AccountFragment();
        }

        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }
}
