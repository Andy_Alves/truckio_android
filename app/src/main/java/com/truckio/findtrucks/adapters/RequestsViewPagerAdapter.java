package com.truckio.findtrucks.adapters;



import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.truckio.findtrucks.fragments.AccountFragment;
import com.truckio.findtrucks.fragments.PostTruckFragment;
import com.truckio.findtrucks.fragments.RequestFragment;
import com.truckio.findtrucks.fragments.requests.OnGoingTripFragment;
import com.truckio.findtrucks.fragments.requests.RequestListFragment;

/**
 * Created by Andrews on 10,August,2019
 */

public class RequestsViewPagerAdapter extends FragmentStatePagerAdapter {

    public RequestsViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int i) {

        switch (i) {
            case 0:
                return new RequestListFragment();
            case 1:
                return new OnGoingTripFragment();
        }

        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }
}
