package com.truckio.findtrucks.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.libraries.places.api.model.AutocompletePrediction;
import com.truckio.findtrucks.R;
import com.truckio.findtrucks.events.PlaceSelectedEvent;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Admin on 15,August,2019
 */

public class PlacesRecyclerAdapter extends RecyclerView.Adapter<PlacesRecyclerAdapter.PlaceViewHolder> {

    private List<AutocompletePrediction> placesList;

    private Context context;

    public PlacesRecyclerAdapter(Context context, List<AutocompletePrediction> placesList) {
        this.placesList = placesList;
        this.context = context;
    }

    @Override
    public PlaceViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_places, viewGroup, false);
        return new PlaceViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PlaceViewHolder placeViewHolder, int i) {
        final int position = i;

        placeViewHolder.textViewPlace.setText(placesList.get(i).getPrimaryText(null));
        placeViewHolder.textViewAddress.setText(placesList.get(i).getSecondaryText(null));

        placeViewHolder.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new PlaceSelectedEvent(placesList.get(position)));
                ((Activity)context).finish();
            }
        });
    }

    @Override
    public int getItemCount() {
        return placesList.size();
    }

    public class PlaceViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_placename)
        public TextView textViewPlace;

        @BindView(R.id.tv_addressname)
        public TextView textViewAddress;

        public View item;

        public PlaceViewHolder(@NonNull View itemView) {
            super(itemView);
            item = itemView;
            ButterKnife.bind(this,itemView);
        }
    }
}
