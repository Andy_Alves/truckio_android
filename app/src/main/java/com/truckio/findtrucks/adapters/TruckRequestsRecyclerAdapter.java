package com.truckio.findtrucks.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.truckio.findtrucks.R;
import com.truckio.findtrucks.enums.MaterialType;
import com.truckio.findtrucks.enums.TruckType;
import com.truckio.findtrucks.events.TruckRequestClickEvent;
import com.truckio.findtrucks.model.TruckRequest;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Admin on 15,August,2019
 */

public class TruckRequestsRecyclerAdapter extends RecyclerView.Adapter<TruckRequestsRecyclerAdapter.PlaceViewHolder> {

    private List<TruckRequest> truckRequests;

    private Context context;
    private PlacesClient placesClient;
    private final static String TAG = "TruckRequestAdapter";

    public TruckRequestsRecyclerAdapter(Context context, List<TruckRequest> truckRequestsList) {
        this.truckRequests = truckRequestsList;
        this.context = context;

        placesClient = Places.createClient(context);
    }

    @Override
    public PlaceViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_requests, viewGroup, false);
        return new PlaceViewHolder(view);
    }

    public void updateList(List<TruckRequest> list) {
        truckRequests.clear();
        truckRequests.addAll(list);
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(@NonNull PlaceViewHolder placeViewHolder, int i) {
        final int position = i;

        TruckRequest truckRequest = truckRequests.get(position);

        String pickUp = truckRequest.pickup_point;
        String dropPoint = truckRequest.drop_point1;

        Log.e(TAG, "onBindViewHolder: pickup name" + pickUp);

        placeViewHolder.textViewPickUpPoint.setText(pickUp);
        placeViewHolder.textViewDropPoint.setText(dropPoint);

        switch (TruckType.valueOf(truckRequest.truck_type)) {

            case OPEN:
                placeViewHolder.ivTruckType.setImageResource(R.drawable.ic_truck_open);
                placeViewHolder.textViewTruckName.setText(truckRequest.tons + " Ton's" + " Open Body");
                break;

            case CONTAINER:
                placeViewHolder.ivTruckType.setImageResource(R.drawable.ic_container);
                placeViewHolder.textViewTruckName.setText(truckRequest.tons + " Ton's" + " Container");
                break;
            case TRAILER:
                placeViewHolder.ivTruckType.setImageResource(R.drawable.ic_truck_trailer);
                placeViewHolder.textViewTruckName.setText(truckRequest.tons + " Ton's" + " Trailer");
                break;
        }

        switch (MaterialType.valueOf(truckRequest.material_type)) {

            case FOOD:
                placeViewHolder.ivMaterialType.setImageResource(R.drawable.ic_food);
                placeViewHolder.textViewMaterialName.setText("Food & Agri");
                break;

            case ELECTRONICS:
                placeViewHolder.ivMaterialType.setImageResource(R.drawable.ic_electronics);
                placeViewHolder.textViewMaterialName.setText("Electronics");
                break;
            case FMCG:
                placeViewHolder.ivMaterialType.setImageResource(R.drawable.ic_fmcg);
                placeViewHolder.textViewMaterialName.setText("FMCG");
                break;
            case MEDICINE:
                placeViewHolder.ivMaterialType.setImageResource(R.drawable.ic_medicine);
                placeViewHolder.textViewMaterialName.setText("Medicine");
                break;
            case BUILDING_MATERIALS:
                placeViewHolder.ivMaterialType.setImageResource(R.drawable.ic_building_material);
                placeViewHolder.textViewMaterialName.setText("Building Material");
                break;
            case SPARES:
                placeViewHolder.ivMaterialType.setImageResource(R.drawable.ic_spares);
                placeViewHolder.textViewMaterialName.setText("Spares");
                break;
        }


        placeViewHolder.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new TruckRequestClickEvent(truckRequests.get(position), position));
            }
        });
    }

    @Override
    public int getItemCount() {
        Log.e(TAG, "getItemCount: request size : " + truckRequests.size()
        );
        return truckRequests.size();
    }

    public class PlaceViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_pickpoint)
        public TextView textViewPickUpPoint;

        @BindView(R.id.tv_droppoint1)
        public TextView textViewDropPoint;

        @BindView(R.id.tv_truckname)
        public TextView textViewTruckName;

        @BindView(R.id.tv_materialname)
        public TextView textViewMaterialName;

        @BindView(R.id.iv_ic_trucktype)
        ImageView ivTruckType;

        @BindView(R.id.iv_ic_materialtype)
        ImageView ivMaterialType;

        public View item;

        public PlaceViewHolder(@NonNull View itemView) {
            super(itemView);
            item = itemView;
            ButterKnife.bind(this,itemView);
        }
    }
}
