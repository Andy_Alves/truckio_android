package com.truckio.findtrucks.model;

/**
 * Created by Andrews on 09,September,2019
 */

public class KycShipper {

    public String userId;
    public String userType;
    public String companyName;
    public String address;
    public String panId;
    public String panFrontUrl;
    public String panBackUrl;
    public String aadharFrontUrl;
    public String aadharBackUrl;

    public KycShipper() {
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPanId() {
        return panId;
    }

    public void setPanId(String panId) {
        this.panId = panId;
    }

    public String getPanFrontUrl() {
        return panFrontUrl;
    }

    public void setPanFrontUrl(String panFrontUrl) {
        this.panFrontUrl = panFrontUrl;
    }

    public String getPanBackUrl() {
        return panBackUrl;
    }

    public void setPanBackUrl(String panBackUrl) {
        this.panBackUrl = panBackUrl;
    }

    public String getAadharFrontUrl() {
        return aadharFrontUrl;
    }

    public void setAadharFrontUrl(String aadharFrontUrl) {
        this.aadharFrontUrl = aadharFrontUrl;
    }

    public String getAadharBackUrl() {
        return aadharBackUrl;
    }

    public void setAadharBackUrl(String aadharBackUrl) {
        this.aadharBackUrl = aadharBackUrl;
    }
}
