package com.truckio.findtrucks.model;

import java.util.List;

/**
 * Created by Admin on 29,August,2019
 */
public class Shipper {

    public String user_id;
    public String name;
    public String company_name;
    public String phone;
    public String city;
    public String password;

    public List<String> truck_request_ids;
    public List<String> ongoing_shipment_ids;
    public List<String> delivered_shipment_ids;

    public int sign_up_timestamp;
    public int last_login_timestamp;

    public Shipper() {
    }

    public Shipper(String userId, String name, String companyName, String phone, String city) {
        this.user_id = userId;
        this.name = name;
        this.company_name = companyName;
        this.phone = phone;
        this.city = city;
    }

    public String getUser_id() {
        return user_id;
    }

    public String getName() {
        return name;
    }

    public String getCompany_name() {
        return company_name;
    }

    public String getPhone() {
        return phone;
    }

    public String getPassword() {
        return password;
    }

    public List<String> getTruck_request_ids() {
        return truck_request_ids;
    }

    public List<String> getOngoing_shipment_ids() {
        return ongoing_shipment_ids;
    }

    public List<String> getDeliveredShipmentIds() {
        return delivered_shipment_ids;
    }

    public int getSignUpTimeStamp() {
        return sign_up_timestamp;
    }

    public int getLast_login_timestamp() {
        return last_login_timestamp;
    }

    public String getCity() {
        return city;
    }
}
