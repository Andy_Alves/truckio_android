package com.truckio.findtrucks.model;

import com.google.android.libraries.places.api.model.AutocompletePrediction;
import com.truckio.findtrucks.enums.RequestStatus;
import com.truckio.findtrucks.enums.TruckType;
import com.truckio.findtrucks.manager.FirestoreManager;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by Andrews on 31,August,2019
 */

public class TruckRequest {

    public String user_id;
    public String request_id;

    public String placeId1;
    public String placeId2;
    public String placeId3;
    public String placeId4;

    public String pickup_point;
    public String drop_point1;
    public String drop_point2;
    public String drop_point3;

    public String tons;
    public String truck_type;
    public String material_type;

    public String status;

    public int price = 14000;

    // 0 month 1 date 2 hours 3 minutes
    public List<Integer> pickuptime;

    public String request_created_timestamp;

    public TruckRequest() {

        user_id = FirestoreManager.getInstance().userId;
        request_id = UUID.randomUUID().toString();
        status = RequestStatus.PENDING.toString();

        pickuptime = new ArrayList<>();
        pickuptime.add(0);
        pickuptime.add(0);
        pickuptime.add(0);
        pickuptime.add(0);
    }

    public String getUser_id() {
        return user_id;
    }

    public String getRequest_id() {
        return request_id;
    }

    public String getPlaceId1() {
        return placeId1;
    }

    public String getPlaceId2() {
        return placeId2;
    }

    public String getPlaceId3() {
        return placeId3;
    }

    public String getPlaceId4() {
        return placeId4;
    }

    public String getPickup_point() {
        return pickup_point;
    }

    public String getDrop_point1() {
        return drop_point1;
    }

    public String getDrop_point2() {
        return drop_point2;
    }

    public String getDrop_point3() {
        return drop_point3;
    }

    public String getTons() {
        return tons;
    }

    public String getTruck_type() {
        return truck_type;
    }

    public String getMaterial_type() {
        return material_type;
    }

    public List<Integer> getPickuptime() {
        return pickuptime;
    }

    public void setRequest_created_timestamp(String request_created_timestamp) {
        this.request_created_timestamp = request_created_timestamp;
    }


    ///////////////
    // UTIL functions
    ///////////////

    public int getMonth() {
        return pickuptime.get(0);
    }

    public int getDate() {
        return pickuptime.get(1);
    }

    public int getHours() {
        return pickuptime.get(2);
    }

    public int getMinutes() {
        return pickuptime.get(3);
    }

    public void setPlaceId1(String placeId1) {
        this.placeId1 = placeId1;
    }

    public void setPlaceId2(String placeId2) {
        this.placeId2 = placeId2;
    }

    public void setPlaceId3(String placeId3) {
        this.placeId3 = placeId3;
    }

    public void setPlaceId4(String placeId4) {
        this.placeId4 = placeId4;
    }

    public void setTons(String tons) {
        this.tons = tons;
    }

    public void setTruck_type(String truck_type) {
        this.truck_type = truck_type;
    }

    public void setMaterial_type(String material_type) {
        this.material_type = material_type;
    }

    public void setPickUpDate(int date, int month) {
        pickuptime.set(1, date);
        pickuptime.set(0, month);
    }

    public void setPickUpTime(int hours, int minutes) {
        pickuptime.set(2, hours);
        pickuptime.set(3, minutes);
    }

    public void setPickUpPoint(AutocompletePrediction place) {
        placeId1 = place.getPlaceId();
        pickup_point = place.getFullText(null).toString();
    }

    public void setDropPoint1(AutocompletePrediction place) {
        placeId2 = place.getPlaceId();
        drop_point1 = place.getFullText(null).toString();
    }

    public void setDropPoint2(AutocompletePrediction place) {
        placeId3 = place.getPlaceId();
        drop_point2 = place.getFullText(null).toString();
    }

    public void setDropPoint3(AutocompletePrediction place) {
        placeId4 = place.getPlaceId();
        drop_point3 = place.getFullText(null).toString();
    }
}
