package com.truckio.findtrucks;

import android.app.Application;

import com.google.android.libraries.places.api.Places;
import com.truckio.findtrucks.manager.FirestoreManager;

/**
 * Created by Andrews on 14,August,2019
 */
public class TruckioApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        Places.initialize(this, "AIzaSyD8hdlHigRpQLzypFNFdcJ7luaIWaF2SSw");
        FirestoreManager.intialize(this);


    }
}
